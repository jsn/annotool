# Annotool API
API for Annotool on Toolforge.

## Development

### Prerequisites
* Install the latest version of [Poetry](https://python-poetry.org/docs/#installation)

### Setup
After installing the prerequisites, run:
```bash
git clone https://gitlab.wikimedia.org/repos/mnz/annotool.git
cd annotool
poetry install --no-root
```
Add your `config.yaml` and then run:
```bash
flask --app annotool init-db
flask --app annotool run
```

### Testing
To run all tests:
```bash
poetry run pytest -vv
```

### Hooks
To set up the pre-commit hooks, run:
```bash
poetry run pre-commit install
```
This will lint and typecheck the code on every commit. Hooks and tests are also run in the CI.
