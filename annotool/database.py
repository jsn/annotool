from dataclasses import dataclass
from typing import Any, Generic, List, Optional, TypeVar

import click

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Row
from sqlalchemy.sql.expression import Select
from typing_extensions import Self


db: SQLAlchemy = SQLAlchemy()


def init_db() -> None:
    db.drop_all()
    db.create_all()


@click.command("init-db")
def init_db_command() -> None:
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


T = TypeVar("T")
U = TypeVar("U", bound=tuple[Any, ...])


@dataclass
class PaginatedResults(Generic[T]):
    data: List[T]
    has_more: bool


class BaseModel(db.Model):  # type: ignore
    __abstract__ = True

    @classmethod
    def get(cls, **kwargs: Any) -> Optional[Self]:
        query = db.select(cls).filter_by(**kwargs)
        result = db.session.execute(query).scalar()
        return result

    @classmethod
    def paginate(
        cls,
        selectable: Select[U],
        limit: int,
        starting_after_id: Optional[int] = None,
    ) -> PaginatedResults[Row[U]]:
        """Paginates the rows in the select statement `selectable`
        using keyset pagination where the key is the id of the model
        """
        query = selectable.order_by(cls.id.desc()).limit(limit + 1)
        if starting_after_id is not None:
            query = query.filter(cls.id < starting_after_id)
        results = list(db.session.execute(query).all())
        has_more = len(results) > limit
        return PaginatedResults(data=results[:limit], has_more=has_more)
