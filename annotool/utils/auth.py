from functools import wraps
from typing import Any, Callable, TypeVar, cast

from flask_login import current_user

from annotool import login_manager, model


F = TypeVar("F", bound=Callable[..., Any])


def admin_login_required(func: F) -> F:
    @wraps(func)
    def decorated_route(*args: Any, **kwargs: Any) -> Any:
        if current_user.is_authenticated and current_user.role == model.Role.ADMIN:
            return func(*args, **kwargs)
        return login_manager.unauthorized()

    return cast(F, decorated_route)
