import flask

from werkzeug.exceptions import HTTPException


def handle_http_exception(e: HTTPException) -> flask.Response:
    response = flask.jsonify(
        {
            "code": e.code,
            "error": e.name,
            "description": e.description,
        }
    )
    response.status_code = 500 if e.code is None else e.code
    return response
