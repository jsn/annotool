import datetime

from typing import Any

from flask.json.provider import DefaultJSONProvider


class CustomJSONProvider(DefaultJSONProvider):
    def default(self, val: Any) -> Any:
        if isinstance(val, (datetime.date, datetime.datetime)):
            return val.isoformat()
        return super().default(val)
