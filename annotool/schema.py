import datetime

from enum import Enum, unique
from typing import Dict, List, Optional

from pydantic import BaseModel, Field
from spectree import BaseFile


class CustomBaseModel(BaseModel):
    class Config:
        frozen = True
        orm_mode = True


class PaginateBase(CustomBaseModel):
    starting_after: Optional[int]
    limit: int = Field(50, gt=0, le=100)


class PaginationBase(CustomBaseModel):
    has_more: bool


class ProjectBase(CustomBaseModel):
    name: str
    description: str
    model: str
    instructions: str
    show_model_result: bool = True


class Project(ProjectBase):
    id: int
    created_at: datetime.datetime
    updated_at: datetime.datetime


class ProjectCreate(ProjectBase):
    pass


class ProjectPaginate(PaginateBase):
    pass


class ProjectList(PaginationBase):
    projects: List[Project]


class ProjectImport(CustomBaseModel):
    file: BaseFile


class LabelBase(CustomBaseModel):
    name: str


class Label(LabelBase):
    id: int
    project: int


class LabelCreate(LabelBase):
    pass


class LabelPaginate(PaginateBase):
    pass


class LabelList(PaginationBase):
    labels: List[Label]


class ReviewBase(CustomBaseModel):
    revision: int
    label: int


class Review(ReviewBase):
    id: int
    reviewer: str
    created_at: datetime.datetime


class ReviewList(PaginationBase):
    reviews: List[Review]


class ReviewCreate(ReviewBase):
    pass


class ReviewUpdate(CustomBaseModel):
    label: int


class ReviewSummarize(CustomBaseModel):
    project: int
    wiki_db: str
    rev: int


class ReviewSummary(ReviewSummarize):
    label_counts: Dict[str, int]


class ReviewPaginate(PaginateBase):
    project: Optional[int]
    wiki_db: Optional[str]
    rev: Optional[int]


class RevisionBase(CustomBaseModel):
    wiki_db: str
    rev: int
    prediction: str
    probability: float


class RevisionCreate(RevisionBase):
    pass


class Revision(RevisionBase):
    id: int
    project: int


class RevisionPaginate(PaginateBase):
    wiki_db: Optional[str]


class RevisionList(PaginationBase):
    revisions: List[Revision]


@unique
class Role(str, Enum):
    ADMIN = "admin"
    USER = "user"


class User(CustomBaseModel):
    username: str
    role: Role
