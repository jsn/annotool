from flask import Blueprint, render_template


client = Blueprint(
    "client",
    __name__,
    template_folder="../frontend/dist",
    static_folder="../frontend/dist/assets",
    static_url_path="/assets",
)


@client.route("/", defaults={"path": ""})
@client.route("/<path:path>")
def catch_all(path: str) -> str:
    return render_template("index.html")
