from pathlib import Path
from typing import List

import flask
import yaml

from pydantic import SecretStr
from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class Config:
    DATABASE_URI: str
    ADMIN_PASSWORD: SecretStr
    MEDIAWIKI_OAUTH_KEY: str
    MEDIAWIKI_OAUTH_SECRET: SecretStr
    MEDIAWIKI_OAUTH_URL: str
    SECRET_KEY: SecretStr
    ADMIN_USERNAMES: List[str]

    @classmethod
    def from_yaml(cls, root_path: Path, filename: str) -> "Config":
        config = flask.Config(str(root_path))
        config.from_file(filename, load=yaml.safe_load)

        return Config(**config)
