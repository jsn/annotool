import spectree

from flask import Blueprint
from flask_login import current_user, login_required
from werkzeug.exceptions import NotFound

from annotool import model, schema, spec
from annotool.database import db


bp: Blueprint = Blueprint("reviews", __name__)


@bp.route("", methods=["POST"])
@spec.validate(resp=spectree.Response(HTTP_200=schema.Review))
@login_required
def create_review(json: schema.ReviewCreate) -> schema.Review:
    review = model.Review(
        revision=json.revision,
        label=json.label,
        reviewer=current_user.username,
    )
    db.session.add(review)
    db.session.commit()
    return schema.Review.from_orm(review)


@bp.route("/<int:review_id>", methods=["PATCH"])
@spec.validate(resp=spectree.Response(HTTP_200=schema.Review))
@login_required
def updateReview(review_id: int, json: schema.ReviewUpdate) -> schema.Review:
    review = model.Review.get(id=review_id, reviewer=current_user.username)
    if review is None:
        raise NotFound(f"Could not find review with id: {review_id} by current user")

    setattr(review, "label", json.label)
    db.session.commit()
    return schema.Review.from_orm(review)


@bp.route("")
@spec.validate(resp=spectree.Response(HTTP_200=schema.ReviewList))
def list_reviews(query: schema.ReviewPaginate) -> schema.ReviewList:
    reviews = db.select(model.Review)
    filters = []
    if query.project is not None:
        filters.append(model.Revision.project == query.project)
    if query.wiki_db is not None:
        filters.append(model.Revision.wiki_db == query.wiki_db)
    if query.rev is not None:
        filters.append(model.Revision.rev == query.rev)
    if filters:
        reviews = reviews.join(
            model.Revision, model.Review.revision == model.Revision.id
        ).filter(*filters)

    results = model.Review.paginate(
        selectable=reviews,
        limit=query.limit,
        starting_after_id=query.starting_after,
    )
    filtered_reviews = [review for review, *_ in results.data]
    return schema.ReviewList(reviews=filtered_reviews, has_more=results.has_more)


@bp.route("/summary")
@spec.validate(resp=spectree.Response(HTTP_200=schema.ReviewSummary))
def summarize_reviews(query: schema.ReviewSummarize) -> schema.ReviewSummary:
    filters = (
        model.Revision.project == query.project,
        model.Revision.wiki_db == query.wiki_db,
        model.Revision.rev == query.rev,
    )
    stmt = (
        db.select(model.Review, db.func.count())
        .join(model.Revision, model.Review.revision == model.Revision.id)
        .filter(*filters)
        .group_by(model.Review.label)
    )
    results = db.session.execute(stmt).all()
    label_counts = dict((review.label, label_count) for review, label_count in results)
    return schema.ReviewSummary(
        project=query.project,
        wiki_db=query.wiki_db,
        rev=query.rev,
        label_counts=label_counts,
    )
