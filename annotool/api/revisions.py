import spectree

from flask import Blueprint
from flask_login import current_user, login_required
from werkzeug.exceptions import NotFound

from annotool import model, schema, spec
from annotool.database import db
from annotool.utils.auth import admin_login_required


bp: Blueprint = Blueprint("revisions", __name__)


@bp.route("", methods=["POST"])
@spec.validate(
    resp=spectree.Response(HTTP_200=schema.Revision),
    security={"auth_CookieAuth": []},
)
@admin_login_required
def create_revision(project_id: int, json: schema.RevisionCreate) -> schema.Revision:
    revision = model.Revision(
        project=project_id,
        rev=json.rev,
        wiki_db=json.wiki_db,
        prediction=json.prediction,
        probability=json.probability,
    )
    db.session.add(revision)
    db.session.commit()
    return schema.Revision.from_orm(revision)


@bp.route("")
@spec.validate(
    resp=spectree.Response(HTTP_200=schema.RevisionList),
    security={"auth_CookieAuth": []},
)
@login_required
def list_revisions(
    project_id: int, query: schema.RevisionPaginate
) -> schema.RevisionList:
    if model.Project.get(id=project_id) is None:
        raise NotFound(f"Could not find project with id: {project_id}")

    username = current_user.username
    user_reviews = db.select(model.Review).filter_by(reviewer=username).subquery()

    # Left outer join between all revisions and reviews created by user
    # to get revisions the user has not reviewed yet
    stmt = db.select(model.Revision).outerjoin(
        user_reviews,
        model.Revision.id == user_reviews.c.revision,
    )
    stmt = stmt.filter(
        model.Revision.project == project_id,
        user_reviews.c.id == None,  # noqa: E711
    )
    if query.wiki_db is not None:
        stmt = stmt.filter(model.Revision.wiki_db == query.wiki_db)

    results = model.Revision.paginate(
        selectable=stmt,
        starting_after_id=query.starting_after,
        limit=query.limit,
    )
    revisions = [revision for revision, *_ in results.data]
    return schema.RevisionList(revisions=revisions, has_more=results.has_more)
