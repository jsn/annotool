import spectree

from flask import Blueprint
from flask_login import current_user, login_required

from annotool import schema, spec


bp: Blueprint = Blueprint("user", __name__)


@bp.route("")
@spec.validate(
    resp=spectree.Response(HTTP_200=schema.User),
    security={"auth_CookieAuth": []},
)
@login_required
def get_current_user() -> schema.User:
    return schema.User(
        username=current_user.username,
        role=current_user.role,
    )
