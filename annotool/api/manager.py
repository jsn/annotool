import codecs
import csv

from typing import IO

from annotool import model
from annotool.database import db


class CSVParseError(Exception):
    def __init__(self, file: str, line: int, message: str) -> None:
        self.file = file
        self.line = line
        self.message = message


class Manager:
    def __init__(self, project_id: int) -> None:
        self.project_id = project_id

    def import_csv(self, filename: str, reader: IO[bytes]) -> None:
        contents = codecs.iterdecode(reader, "utf-8")
        csv_reader = csv.DictReader(contents, delimiter="\t")
        revisions = []
        for row_num, row in enumerate(csv_reader, start=1):
            try:
                revision = model.Revision(
                    rev=row["rev"],
                    wiki_db=row["wiki_db"],
                    project=self.project_id,
                    prediction=row["prediction"],
                    probability=row["probability"],
                )
                revisions.append(revision)

            except KeyError as e:
                raise CSVParseError(
                    file=filename,
                    line=row_num,
                    message=f"'{e.args[0]}' column missing",
                )

        db.session.bulk_save_objects(revisions)
        db.session.commit()

    def export_csv(self, writer: IO[str]) -> None:
        stmt = db.select(model.Revision, model.Label).filter(
            model.Review.revision == model.Revision.id,
            model.Revision.project == self.project_id,
            model.Review.label == model.Label.id,
        )
        results = db.session.execute(stmt).all()
        csv_fields = ("wiki_db", "rev", "prediction", "probability", "label")
        csv_writer = csv.DictWriter(writer, fieldnames=csv_fields, delimiter="\t")
        csv_writer.writeheader()
        for revision, label in results:
            csv_writer.writerow(
                {
                    "wiki_db": revision.wiki_db,
                    "rev": revision.rev,
                    "prediction": revision.prediction,
                    "probability": revision.probability,
                    "label": label.name,
                }
            )
