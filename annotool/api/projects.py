import tempfile

import spectree

from flask import Blueprint, Response, request, send_file
from werkzeug.exceptions import BadRequest, NotFound
from werkzeug.utils import secure_filename

from annotool import model, schema, spec
from annotool.api.manager import CSVParseError, Manager
from annotool.database import db
from annotool.utils.auth import admin_login_required


bp: Blueprint = Blueprint("projects", __name__)


@bp.route("", methods=["POST"])
@spec.validate(
    resp=spectree.Response(HTTP_200=schema.Project),
    security={"auth_CookieAuth": []},
)
@admin_login_required
def create_project(json: schema.ProjectCreate) -> schema.Project:
    project = model.Project(
        name=json.name,
        description=json.description,
        model=json.model,
        instructions=json.instructions,
        show_model_result=json.show_model_result,
    )
    db.session.add(project)
    db.session.commit()
    return schema.Project.from_orm(project)


@bp.route("")
@spec.validate(resp=spectree.Response(HTTP_200=schema.ProjectList))
def list_projects(query: schema.ProjectPaginate) -> schema.ProjectList:
    results = model.Project.paginate(
        selectable=db.select(model.Project),
        starting_after_id=query.starting_after,
        limit=query.limit,
    )
    projects = [project for project, *_ in results.data]
    return schema.ProjectList(projects=projects, has_more=results.has_more)


@bp.route("/<int:project_id>")
@spec.validate(resp=spectree.Response(HTTP_200=schema.Project))
def get_project(project_id: int) -> schema.Project:
    project = model.Project.get(id=project_id)
    if project is None:
        raise NotFound(f"Could not find project with id: {project_id}")
    return schema.Project.from_orm(project)


@bp.route("/<int:project_id>/import", methods=["POST"])
@spec.validate(
    form=schema.ProjectImport,
    resp=spectree.Response("HTTP_200"),
    security={"auth_CookieAuth": []},
)
@admin_login_required
def import_dataset(project_id: int) -> Response:
    project = model.Project.get(id=project_id)
    if project is None:
        raise NotFound(f"Could not find project with id: {project_id}")

    dataset_file = request.files["file"]
    if not dataset_file.filename:
        raise BadRequest("No file selected")

    manager = Manager(project_id)
    try:
        manager.import_csv(
            filename=secure_filename(dataset_file.filename),
            reader=dataset_file.stream,
        )
    except CSVParseError as e:
        raise BadRequest(
            f"CSV parse error: line {e.line} in {e.file}: {e.message}",
        )

    return Response(status=200)


@bp.route("/<int:project_id>/export")
@spec.validate(
    resp=spectree.Response("HTTP_200"),
    security={"auth_CookieAuth": []},
)
@admin_login_required
def export_dataset(project_id: int) -> Response:
    project = model.Project.get(id=project_id)
    if project is None:
        raise NotFound(f"Could not find project with id: {project_id}")

    manager = Manager(project_id)
    with tempfile.NamedTemporaryFile(mode="w", suffix=".tsv") as tmpf:
        manager.export_csv(tmpf)
        tmpf.flush()
        return send_file(
            tmpf.name,
            mimetype="text/csv",
            as_attachment=True,
            download_name=f"project_{project_id}_export.tsv",
        )
