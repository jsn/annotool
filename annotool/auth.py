from typing import Any

import flask
import werkzeug

from flask_login import login_required, login_user, logout_user

from annotool import model, oauth


auth = flask.Blueprint("auth", __name__)


@auth.route("/login")
def login() -> Any:
    redirect_uri = flask.url_for("auth.oauth2_callback", _external=True)
    return oauth.mediawiki.authorize_redirect(redirect_uri)


@auth.route("/oauth2-callback")
def oauth2_callback() -> werkzeug.Response:
    oauth.mediawiki.authorize_access_token()
    profile_url = "oauth2/resource/profile"
    profile_response = oauth.mediawiki.get(profile_url)
    profile_response.raise_for_status()
    profile = profile_response.json()

    user = model.User(profile["username"])
    login_user(user)

    return flask.redirect("/")


@auth.route("/logout")
@login_required
def logout() -> werkzeug.Response:
    logout_user()
    return flask.redirect("/")
