import io

from http import HTTPStatus

from flask.testing import FlaskClient
from flask_login import login_user

from annotool import model
from annotool.database import db


base_url = "/api/projects"


def test_create_project(client: FlaskClient, example_admin: model.User) -> None:
    body = {
        "name": "Test Project",
        "description": "Test description.",
        "model": "Test model",
        "instructions": "Test instructions",
        "show_model_result": True,
    }
    with client.application.test_request_context():
        login_user(example_admin)
        response = client.post(base_url, json=body)
        assert response.status_code == HTTPStatus.OK
        created_project = response.json
        assert created_project is not None
        assert created_project["name"] == body["name"]


def test_create_project_without_login(client: FlaskClient) -> None:
    body = {
        "name": "Test Project",
        "description": "Test description.",
        "model": "Test model",
        "instructions": "Test instructions",
        "show_model_result": True,
    }
    response = client.post(base_url, json=body)
    assert response.status_code == HTTPStatus.UNAUTHORIZED


def test_list_projects(client: FlaskClient) -> None:
    projects = (
        model.Project(
            name="Project 1",
            description="Description 1.",
            model="Model 1",
            instructions="Test instructions 1",
            show_model_result=False,
        ),
        model.Project(
            name="Project 2",
            description="Description 2.",
            model="Model 2",
            instructions="Test instructions 2",
            show_model_result=True,
        ),
    )
    db.session.add_all(projects)
    db.session.commit()

    response = client.get(base_url)
    assert response.status_code == HTTPStatus.OK
    project_list = response.json
    assert project_list is not None
    assert len(project_list["projects"]) > 1


def test_get_existing_project(
    client: FlaskClient, example_project: model.Project
) -> None:
    response = client.get(f"{base_url}/{example_project.id}")
    assert response.status_code == HTTPStatus.OK
    retrieved_project = response.json
    assert retrieved_project is not None
    assert retrieved_project["name"] == example_project.name


def test_get_non_existent_project(client: FlaskClient) -> None:
    project_id = 8787
    project = model.Project.get(id=project_id)
    assert project is None

    response = client.get(f"{base_url}/{project_id}")
    assert response.status_code == HTTPStatus.NOT_FOUND


def test_import_dataset(
    client: FlaskClient, example_admin: model.User, example_project: model.Project
) -> None:
    tsv = io.BytesIO(
        b"""wiki_db	rev	prediction	probability
afwiki	12456	not reverted	0.33
enwiki	12345	reverted	0.75
"""
    )
    data = {"file": (tsv, "revisions.tsv")}
    with client.application.test_request_context():
        login_user(example_admin)
        response = client.post(
            f"{base_url}/{example_project.id}/import",
            data=data,
            content_type="multipart/form-data",
        )
        assert response.status_code == HTTPStatus.OK


def test_export_dataset(
    client: FlaskClient, example_admin: model.User, example_project: model.Project
) -> None:
    with client.application.test_request_context():
        login_user(example_admin)
        response = client.get(f"{base_url}/{example_project.id}/export")
        assert response.status_code == HTTPStatus.OK
        assert response.content_type == "text/csv; charset=utf-8"
        filename = f"project_{example_project.id}_export.tsv"
        header = f"attachment; filename={filename}"
        assert response.headers["Content-Disposition"] == header
