import { Box, Typography } from "@mui/material";
import { useEffect } from "react";

export default function Login(): JSX.Element {
  useEffect(() => {
    window.location.replace("/login");
  });

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: `calc(100vh - 64px)`,
      }}
    >
      <Typography variant="h5">Redirecting...</Typography>
    </Box>
  );
}
