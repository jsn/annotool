import { Table, TableBody } from "@mui/material";

type Props = {
  diffHtml: string;
};

export default function RevisionDiff({ diffHtml }: Props): JSX.Element {
  return (
    <div>
      <Table className="diff">
        <colgroup>
          <col className="diff-marker" />
          <col className="diff-content" />
          <col className="diff-marker" />
          <col className="diff-content" />
        </colgroup>
        <TableBody
          className="diff-content"
          dangerouslySetInnerHTML={{ __html: diffHtml }}
        />
      </Table>
    </div>
  );
}
