import { Alert, Box, Button, Container, Stack, Typography } from "@mui/material";
import React from "react";
import ProjectCard from "./ProjectCard";
import { useProjectList } from "../queries";

export default function ProjectPanel(): JSX.Element {
  const {
    data: paginatedProjects,
    error,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    isLoading,
  } = useProjectList();

  return (
    <Container sx={{ py: 6 }} maxWidth="md">
      <Typography variant="h5" fontWeight={500} mb={4}>
        Projects
      </Typography>
      {isLoading ? (
        <></>
      ) : error ? (
        <Alert severity="error">{error.message}</Alert>
      ) : (
        <Stack spacing={4} alignItems={"center"} justifyContent={"center"}>
          {paginatedProjects?.pages.map((group, idx) => (
            <React.Fragment key={idx}>
              {group.projects.map((project) => (
                <Box key={project.id} width={"100%"}>
                  <ProjectCard project={project} />
                </Box>
              ))}
            </React.Fragment>
          ))}

          <Button
            variant="text"
            size="large"
            disabled={!hasNextPage || isFetchingNextPage}
            onClick={() => fetchNextPage()}
          >
            {isFetchingNextPage
              ? "Loading More"
              : hasNextPage
              ? "Load More"
              : "Nothing more to load"}
          </Button>
        </Stack>
      )}
    </Container>
  );
}
