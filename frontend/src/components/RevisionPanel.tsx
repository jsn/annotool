import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Box,
  Button,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import { Fragment } from "react";
import { useParams } from "react-router-dom";
import RevisionCard from "./RevisionCard";
import { useLabels, useProject, useRevisionList } from "../queries";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

function Instructions({ instructions }: { instructions: string }): JSX.Element {
  return (
    <Accordion
      elevation={0}
      square
      sx={{
        border: 1,
        borderColor: "primary.main",
        borderRadius: "4px",
        width: "100%",
      }}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon color="primary" />}
        id="instructions"
      >
        <Typography color="primary">Labeling Instructions</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography color="text.secondary">{instructions}</Typography>
      </AccordionDetails>
    </Accordion>
  );
}

export default function RevisionPanel(): JSX.Element {
  const { projectId: projectIdStr } = useParams() as { projectId: string };
  const projectId = parseInt(projectIdStr);

  const {
    data: project,
    isLoading: isProjLoading,
    error: projError,
  } = useProject(projectId);

  const {
    data: labels,
    isLoading: isLblsLoading,
    error: lblsError,
  } = useLabels(projectId);

  const {
    data: paginatedRevisions,
    error: rvError,
    fetchNextPage: fetchNextRvPage,
    hasNextPage: hasNextRvPage,
    isFetchingNextPage: isFetchingNextRvPage,
    isLoading: isRvLoading,
  } = useRevisionList(projectId);

  const isLoading = isRvLoading || isProjLoading || isLblsLoading;
  const error = rvError || projError || lblsError;

  return (
    <Container sx={{ py: 6 }} maxWidth="lg">
      <Box display="flex" justifyContent={"space-between"} alignItems="center" mb={4}>
        <Typography variant="h5" fontWeight={500}>
          Revisions
        </Typography>
      </Box>
      {isLoading ? (
        <></>
      ) : error ? (
        <Alert severity="error">{error.message}</Alert>
      ) : (
        <Stack spacing={4} alignItems={"center"}>
          {project && (
            <Fragment>
              <Instructions instructions={project.instructions} />
              {paginatedRevisions?.pages.map((group, idx) => (
                <Fragment key={idx}>
                  {group.revisions.map((revision) => (
                    <RevisionCard
                      key={revision.id}
                      revision={revision}
                      labels={labels ?? []}
                      project={project}
                    />
                  ))}
                </Fragment>
              ))}
            </Fragment>
          )}

          <Button
            variant="text"
            size="large"
            disabled={!hasNextRvPage || isFetchingNextRvPage}
            onClick={() => fetchNextRvPage()}
          >
            {isFetchingNextRvPage
              ? "Loading More"
              : hasNextRvPage
              ? "Load More"
              : "Nothing more to load"}
          </Button>
        </Stack>
      )}
    </Container>
  );
}
