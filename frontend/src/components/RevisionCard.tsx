import TaskAltOutlinedIcon from "@mui/icons-material/TaskAltOutlined";
import {
  Alert,
  Box,
  Button,
  ButtonGroup,
  Card,
  Chip,
  Divider,
  Link,
  Skeleton,
  Slide,
  Snackbar,
  Stack,
  Typography,
  alpha,
  useTheme,
} from "@mui/material";
import { useState } from "react";
import { Label } from "../api/labels";
import { Project } from "../api/projects";
import { Review, createReview, updateReview } from "../api/reviews";
import { Revision } from "../api/revisions";
import { useRevDiff, useRevMetadata } from "../queries";
import { wikiToDomain } from "../utils";
import ModelResult from "./ModelResult";
import RevisionDiff from "./RevisionDiff";

type LinkChipProps = {
  label: string;
  href: string;
};

function LinkChip(props: LinkChipProps): JSX.Element {
  const { label, href } = props;
  const theme = useTheme();

  return (
    <Box component={Link} href={href} underline="none" target="_blank" rel="noopener">
      <Chip
        variant="outlined"
        size="small"
        color="primary"
        label={label}
        sx={{
          cursor: "pointer",
          "&:hover": { backgroundColor: alpha(theme.palette.primary.light, 0.05) },
        }}
      />
    </Box>
  );
}

type RevisionCardProps = {
  revision: Revision;
  labels: Array<Label>;
  project: Project;
};

export default function RevisionCard(props: RevisionCardProps): JSX.Element {
  const { revision, labels, project } = props;
  const idToLabel = Object.fromEntries(labels.map((label) => [label.id, label.name]));
  const [alert, setAlert] = useState<string | null>(null);
  const [review, setReview] = useState<Review>();
  const [reviewBarState, setReviewBarState] = useState<{
    visible: boolean;
    enabled: boolean;
  }>({ visible: true, enabled: true });

  const {
    data: diff,
    isLoading: isDiffLoading,
    error: diffError,
  } = useRevDiff(revision);

  const {
    data: metadata,
    isLoading: isMdLoading,
    error: mdError,
  } = useRevMetadata(revision);

  const isLoading = isMdLoading || isDiffLoading;
  const error = mdError || diffError;

  async function handleCreateReview(label: Label) {
    setReviewBarState({ ...reviewBarState, enabled: false });

    const reviewPromise: Promise<Review> =
      review === undefined
        ? createReview(revision.id, label.id)
        : updateReview(review.id, label.id);

    reviewPromise
      .then((review) => {
        setReview(review);
        setReviewBarState({ ...reviewBarState, visible: false });
      })
      .catch((err) => {
        setAlert(err.message);
        setReviewBarState({ ...reviewBarState, enabled: true });
      });
  }

  return (
    <>
      {isLoading ? (
        <Skeleton variant="rounded" animation="wave" height="20em" width="100%" />
      ) : error ? (
        <Alert severity="error" sx={{ width: "100%", boxSizing: "border-box" }}>
          {error.message}
        </Alert>
      ) : (
        <Card elevation={0} sx={{ border: 1, borderColor: "primary.main" }}>
          <Box sx={{ padding: 4 }}>
            <Stack direction="row" alignItems="center" spacing={1} mb={0.5}>
              <Link
                href={metadata?.pageLink}
                variant="h6"
                color="primary"
                underline="hover"
                target="_blank"
                rel="noopener"
              >
                {metadata?.pageTitle}
              </Link>
              <LinkChip
                label={revision.wiki_db}
                href={`https://${wikiToDomain(revision.wiki_db)}`}
              />
              <LinkChip
                href={`${metadata?.pageLink}?oldid=${revision.rev}`}
                label={revision.rev.toString()}
              />
            </Stack>
            <Typography variant="body1" color="text.secondary">
              {metadata?.pageExtract}
            </Typography>
            {project.show_model_result && (
              <ModelResult
                model={project.model}
                prediction={revision.prediction}
                probability={revision.probability}
              />
            )}
          </Box>
          <Divider />
          <Box sx={{ padding: 4 }}>
            <Stack spacing={4} alignItems={"center"}>
              {diff && <RevisionDiff diffHtml={diff} />}
              {reviewBarState.visible ? (
                <ButtonGroup
                  size="small"
                  variant="outlined"
                  aria-label="outlined primary button group"
                  disabled={!reviewBarState.enabled}
                >
                  {labels.map((label) => (
                    <Button
                      disableElevation
                      variant="outlined"
                      key={label.id}
                      onClick={() => handleCreateReview(label)}
                    >
                      {label.name}
                    </Button>
                  ))}
                </ButtonGroup>
              ) : (
                review && (
                  <Slide in={review !== undefined} direction="up" mountOnEnter>
                    <Stack>
                      <Box display="flex" alignItems="center" color="success.main">
                        <TaskAltOutlinedIcon sx={{ mr: 1 }} />
                        <Typography>
                          Marked as <b>{idToLabel[review.label]}</b>
                        </Typography>
                      </Box>
                      <Button
                        disableElevation
                        variant="text"
                        size="small"
                        sx={{
                          padding: 0,
                          minHeight: 0,
                          minWidth: 0,
                          backgroundColor: "transparent",
                          "&:hover": { background: "none" },
                        }}
                        onClick={() =>
                          setReviewBarState({ visible: true, enabled: true })
                        }
                      >
                        Edit
                      </Button>
                    </Stack>
                  </Slide>
                )
              )}
              <Snackbar
                open={alert !== null}
                autoHideDuration={5000}
                onClose={() => setAlert(null)}
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
              >
                <Alert severity="error">{alert}</Alert>
              </Snackbar>
            </Stack>
          </Box>
        </Card>
      )}
    </>
  );
}
