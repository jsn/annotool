import { Box, Stack, Typography, alpha, useTheme } from "@mui/material";

type SplitCardProps = {
  leftText: string;
  rightText: string;
};

function SplitCard(props: SplitCardProps): JSX.Element {
  const theme = useTheme();
  const { leftText, rightText } = props;

  return (
    <Box
      sx={{
        display: "flex",
        overflow: "hidden",
        borderRadius: "4px",
        backgroundColor: alpha(theme.palette.primary.main, 0.1),
      }}
    >
      <Typography
        color="text.secondary"
        sx={{
          padding: "5px",
          height: "100%",
          backgroundColor: alpha(theme.palette.primary.main, 0.15),
          borderRight: "1px solid",
          borderColor: "primary.main",
        }}
      >
        {leftText}
      </Typography>
      <Typography color="text.secondary" sx={{ padding: "5px" }}>
        {rightText}
      </Typography>
    </Box>
  );
}

type ModelResultProps = {
  model: string;
  prediction: string;
  probability: number;
};

export default function ModelResult(props: ModelResultProps): JSX.Element {
  const { model, prediction, probability } = props;

  return (
    <Stack spacing={0.5} mt={1.5}>
      <Typography
        color="text.secondary"
        variant="subtitle1"
        fontWeight={500}
        sx={{ opacity: 0.5 }}
      >
        {model}
      </Typography>
      <Stack direction="row" spacing={1.5}>
        <SplitCard leftText="prediction" rightText={prediction} />
        <SplitCard leftText="probability" rightText={`${probability}`} />
      </Stack>
    </Stack>
  );
}
