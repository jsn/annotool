import AccountCircle from "@mui/icons-material/AccountCircle";
import CreateOutlinedIcon from "@mui/icons-material/CreateOutlined";
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useUser } from "../queries";

export default function Header(): JSX.Element {
  const { data: user } = useUser();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const isMenuOpen = Boolean(anchorEl);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleSignOut = () => {
    window.location.replace("/logout");
  };

  const renderMenu = (
    <Menu
      sx={{ mt: "30px" }}
      elevation={1}
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id="account-menu"
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleSignOut}>Sign Out</MenuItem>
    </Menu>
  );

  return (
    <>
      <AppBar position="static" elevation={0}>
        <Container maxWidth="xl">
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <Box display="flex" alignItems="center">
              <CreateOutlinedIcon sx={{ mr: 1, display: { xs: "none", md: "flex" } }} />
              <Typography
                variant="h6"
                component={Link}
                to="/"
                sx={{
                  mr: 2,
                  color: "inherit",
                  fontFamily: "monospace",
                  fontWeight: 700,
                  letterSpacing: ".2rem",
                  textDecoration: "none",
                }}
              >
                ANNOTOOL
              </Typography>
            </Box>
            {user ? (
              <>
                <Button
                  variant="text"
                  onClick={handleMenuOpen}
                  color="inherit"
                  startIcon={<AccountCircle />}
                  sx={{
                    textTransform: "none",
                    backgroundColor: "transparent",
                    display: { xs: "none", md: "flex" },
                    "&:hover": { background: "none" },
                  }}
                >
                  {user.username}
                </Button>
                <IconButton
                  onClick={handleMenuOpen}
                  color="inherit"
                  sx={{
                    display: { xs: "flex", md: "none" },
                  }}
                >
                  <AccountCircle />
                </IconButton>
              </>
            ) : (
              <Typography
                component={Link}
                to="/login"
                sx={{
                  color: "inherit",
                  fontWeight: 500,
                  letterSpacing: ".1rem",
                  textDecoration: "none",
                }}
              >
                Sign In
              </Typography>
            )}
          </Toolbar>
        </Container>
      </AppBar>
      {renderMenu}
    </>
  );
}
