import {
  UseInfiniteQueryResult,
  UseQueryResult,
  useInfiniteQuery,
  useQuery,
} from "@tanstack/react-query";
import { Project, PaginatedProjects, getProject, listProjects } from "./api/projects";
import { Label, getLabels } from "./api/labels";
import { PaginatedRevisions, Revision, listRevisions } from "./api/revisions";
import { RevMetadata, getRevDiff, getRevMetadata } from "./api/mediawiki";
import { User, getLoggedInUser } from "./api/user";

export function useProject(projectId: number): UseQueryResult<Project, Error> {
  return useQuery<Project, Error>({
    queryKey: ["project"],
    queryFn: () => getProject(projectId),
    refetchOnWindowFocus: false,
  });
}

export function useLabels(projectId: number): UseQueryResult<Array<Label>, Error> {
  return useQuery<Array<Label>, Error>({
    queryKey: ["labels"],
    queryFn: () => getLabels(projectId),
    refetchOnWindowFocus: false,
  });
}

export function useRevMetadata(revision: Revision): UseQueryResult<RevMetadata, Error> {
  return useQuery<RevMetadata, Error>({
    queryKey: ["revMetadata", revision.id],
    queryFn: () => getRevMetadata(revision.rev, revision.wiki_db),
    refetchOnWindowFocus: false,
  });
}

export function useRevDiff(revision: Revision): UseQueryResult<string, Error> {
  return useQuery<string, Error>({
    queryKey: ["revDiff", revision.id],
    queryFn: () => getRevDiff(revision.rev, revision.wiki_db),
    refetchOnWindowFocus: false,
  });
}

export function useUser(): UseQueryResult<User, Error> {
  return useQuery<User, Error>({
    queryKey: ["user"],
    queryFn: getLoggedInUser,
    refetchOnWindowFocus: false,
    retry: false,
  });
}

function getNextProjectPageParam(lastPage: PaginatedProjects): number | undefined {
  if (!lastPage.has_more) {
    return undefined;
  }
  const lastProject = lastPage.projects.length - 1;
  return lastPage.projects[lastProject].id;
}

export function useProjectList(): UseInfiniteQueryResult<PaginatedProjects, Error> {
  return useInfiniteQuery<PaginatedProjects, Error>({
    queryKey: ["projects"],
    queryFn: ({ pageParam = undefined }) => listProjects(pageParam),
    getNextPageParam: getNextProjectPageParam,
    refetchOnWindowFocus: false,
  });
}

function getNextRevisionsPageParam(lastPage: PaginatedRevisions): number | undefined {
  if (!lastPage.has_more) {
    return undefined;
  }
  const lastRevision = lastPage.revisions.length - 1;
  return lastPage.revisions[lastRevision].id;
}

export function useRevisionList(
  projectId: number,
  wikiDb?: string
): UseInfiniteQueryResult<PaginatedRevisions, Error> {
  return useInfiniteQuery<PaginatedRevisions, Error>({
    queryKey: ["revisions", wikiDb],
    queryFn: ({ pageParam = undefined }) => {
      return listRevisions(projectId, wikiDb, pageParam);
    },
    getNextPageParam: getNextRevisionsPageParam,
    refetchOnWindowFocus: false,
  });
}
