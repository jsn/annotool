import { absolutifyLinks, handleUnsuccessfulResponse, wikiToDomain } from "../utils";

export type RevMetadata = {
  parentRev: number;
  user: string;
  pageLink: string;
  pageTitle?: string;
  pageExtract?: string;
};

export async function getWikiRevMetadata(
  rev: number,
  wikiDb: string
): Promise<RevMetadata> {
  const domain = wikiToDomain(wikiDb);
  const params = {
    action: "query",
    format: "json",
    formatversion: "2",
    prop: "revisions|extracts",
    revids: rev.toString(),
    rvslots: "main",
    rvprop: "user|ids",
    exintro: "1",
    explaintext: "1",
    exsentences: "1",
    origin: "*",
  };
  const queryParams = new URLSearchParams(params);
  const response = await fetch(`https://${domain}/w/api.php?` + queryParams);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  const data = await response.json();
  const page = data["query"]?.["pages"]?.[0];
  if (page === undefined) {
    throw Error(`Couldn't fetch revision ${rev} in ${wikiDb}`);
  }
  const revInfo = page["revisions"][0];
  const pageSlug = page["title"].replace(/ /g, "_");
  return {
    parentRev: revInfo["parentid"],
    user: revInfo["user"],
    pageLink: `https://${domain}/wiki/${pageSlug}`,
    pageTitle: page["title"],
    pageExtract: page["extract"],
  };
}

export async function getWikibaseRevMetadata(rev: number): Promise<RevMetadata> {
  const domain = wikiToDomain("wikidata");
  const params = {
    action: "query",
    format: "json",
    formatversion: "2",
    prop: "revisions",
    revids: rev.toString(),
    rvslots: "main",
    rvprop: "user|ids",
    origin: "*",
  };
  const revResp = await fetch(
    `https://${domain}/w/api.php?` + new URLSearchParams(params)
  );
  if (!revResp.ok) {
    handleUnsuccessfulResponse(revResp);
  }
  const revData = await revResp.json();
  const page = revData["query"]?.["pages"]?.[0];
  if (page === undefined) {
    throw Error(`Couldn't fetch revision ${rev} in wikidata`);
  }
  const qid = page["title"];
  const parentRev = page["revisions"][0]["parentid"];

  // Fetch data for entity at parent rev so that it reflects
  // what the entity looked like when the revision was made
  const entityResp = await fetch(
    `https://${domain}/wiki/Special:EntityData/${qid}.json?revision=${parentRev}`
  );
  if (!entityResp.ok) {
    handleUnsuccessfulResponse(entityResp);
  }
  const entityData = await entityResp.json();
  const entity = entityData?.["entities"]?.[qid];
  if (entity === undefined) {
    throw Error(`Couldn't fetch entity ${qid}`);
  }

  return {
    parentRev,
    user: page["revisions"][0]["user"],
    pageLink: `https://${domain}/wiki/${qid}`,
    pageTitle: entity["labels"]["en"]?.["value"],
    pageExtract: entity["descriptions"]["en"]?.["value"],
  };
}

export async function getRevMetadata(
  rev: number,
  wikiDb: string
): Promise<RevMetadata> {
  switch (wikiDb) {
    case "wikidata":
      return getWikibaseRevMetadata(rev);
    default:
      return getWikiRevMetadata(rev, wikiDb);
  }
}

function fixDiffLinks(diffHtml: string, wikiDb: string): string {
  const parser = new DOMParser();
  // wrapping the diff since DOMParser strips out invalid html
  // and the orphaned tr nodes in the diff are invalid
  const wrappedDiff = "<table><tbody>" + diffHtml + "</tbody></table>";
  const document = parser.parseFromString(wrappedDiff, "text/html");
  absolutifyLinks(document, "https://" + wikiToDomain(wikiDb));
  return (document.body.firstChild?.firstChild as HTMLElement).innerHTML;
}

export async function getRevDiff(rev: number, wikiDb: string): Promise<string> {
  const baseUrl = `https://${wikiToDomain(wikiDb)}/w/api.php?`;
  const params = {
    action: "compare",
    format: "json",
    formatversion: "2",
    fromrev: rev.toString(),
    torelative: "prev",
    origin: "*",
  };
  const queryParams = new URLSearchParams(params);
  const response = await fetch(baseUrl + queryParams);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  const data = await response.json();
  if (data["compare"] === undefined) {
    throw Error(`Couldn't fetch diff for ${rev} in ${wikiDb}`);
  }
  const fixedDiff = fixDiffLinks(data["compare"]["body"], wikiDb);
  return fixedDiff;
}
