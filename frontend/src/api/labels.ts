import { handleUnsuccessfulResponse } from "../utils";

export type Label = {
  id: number;
  name: string;
  project: number;
};

export async function getLabels(projectId: number): Promise<Array<Label>> {
  const response = await fetch(`/api/projects/${projectId}/labels`);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  const data = await response.json();
  return data["labels"];
}
